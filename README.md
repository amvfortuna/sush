# Sush Mobile Coding Exercise #

To start:

* Clone the project 
* Open the `.xcworkspace`
* Press `Run`. It should work with no problem.

To disable logging, please change the scheme from "Debug" to "Release".

Cheers!