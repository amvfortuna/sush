//
//  PhotosCollectionViewCell.swift
//  Sush
//
//  Created by Anna Fortuna on 1/27/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.thumbnail.image = nil
    }
}
