//
//  Alerts.swift
//  Sush
//
//  Created by Anna Fortuna on 1/28/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import UIKit

final class Alerts {
    
    class func noInternet() -> UIAlertController {
        return Alerts.createAlert(Text.Error.NoInternetTitle, message: Text.Error.NoInternetBody)
    }
    
    class func requestTimeOut() -> UIAlertController {
        return Alerts.createAlert(Text.Error.RequestTimeoutTitle, message: Text.Error.RequestTimeoutBody)
    }
    
    class func serverError() -> UIAlertController {
        return Alerts.createAlert(Text.Error.ServerErrorTitle, message: Text.Error.ServerErrorBody)
    }
    
    class func invalidData() -> UIAlertController {
        return Alerts.createAlert(Text.Error.InvalidDataTitle, message: Text.Error.InvalidDataBody)
    }
    
    class func invalidURL() -> UIAlertController {
        return Alerts.createAlert(Text.Error.InvalidURLTitle, message: Text.Error.InvalidURLBody)
    }
    
    class func otherError() -> UIAlertController {
        return Alerts.createAlert(Text.Error.OtherErrorTitle, message: Text.Error.OtherErrorBody)
    }
    
    class func createAlert(_ title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        return alert
    }
}
