//
//  Extensions.swift
//  Sush
//
//  Created by Anna Fortuna on 1/28/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import UIKit

extension UIColor {
    class var sushBlue: UIColor {
        return UIColor(colorLiteralRed: 0, green: (113/255), blue: (179/255), alpha: 1.0)
    }
}
