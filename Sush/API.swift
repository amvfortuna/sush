//
//  API.swift
//  Sush
//
//  Created by Anna Fortuna on 1/27/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import UIKit
import HTTPStatusCodes

typealias InfoDictionary = Dictionary<String, Any>
typealias APIResult = (_ info: [InfoDictionary]?, _ error: APIError?) -> Void

enum APIError {
    case noInternet, requestTimeOut, serverError, invalidData, invalidURL, otherError
}

final class API {
    struct EndPoints {
        private(set) var Albums: String
        private(set) var Users: String
        private(set) var Photos: String
        
        init(values: InfoDictionary) {
            let base = values["base"] as! String
            self.Albums = base + (values["albumsEndpoint"] as? String ?? "INVALID_ALBUM_ENDPOINT")
            self.Users = base + (values["usersEndpoint"] as? String ?? "INVALID_USERS_ENDPOINT" )
            self.Photos = base + (values["photosEndpoint"] as? String ?? "INVALID_PHOTOS_ENDPOINT" )
        }
    }

    let endpoint: EndPoints!
    fileprivate let session: URLSession!
    
    init() {
        let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as! String
        let configPlistPath = Bundle.main.path(forResource: configuration, ofType: "plist")
        assert((configPlistPath != nil), "Configuration Property list is missing.")
        
        self.endpoint = EndPoints(values: (NSDictionary(contentsOfFile: configPlistPath!)) as! InfoDictionary)
        self.session = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    func getAlbums(completion: @escaping APIResult) {
        self.get(self.endpoint.Albums) { info, error in
            completion(info, error)
        }
    }
    
    func getUsers(completion: @escaping APIResult ) {
        self.get(self.endpoint.Users) { info, error in
            completion(info, error)
        }
    }
    
    func getAlbumsWithUsers(completion: @escaping APIResult) {
        self.get(self.endpoint.Albums) { albumList, albumError in
            guard let _albumList = albumList, albumError == nil else {
                completion(nil, albumError)
                return
            }
            self.get(self.endpoint.Users) { userList, userError in
                guard let _userList = userList, userError == nil else {
                    completion(nil, userError)
                    return
                }
                let aggregatedInfo = _albumList.map { album -> (InfoDictionary) in
                    var _album = album
                    _album["user_name"] = ""
                    userLoop: for user in _userList {
                        if (user["id"] as? Int)! == (album["userId"] as? Int)! {
                            _album["user_name"] = (user["name"] as! String)
                            break userLoop
                        }
                    }
                    return _album
                }
                completion(aggregatedInfo, nil)
            }
        }
    }
    
    func getPhotosFromAlbum(id: Int, completion: @escaping APIResult) {
        let link = self.endpoint.Photos + "\(id)"
        self.get(link) { info, error in
            completion(info, error)
        }
    }
    
    func get(_ link: String, completion: @escaping APIResult) {
        self.requestExists(link) { [weak self] exists in
            guard let strongSelf = self else {
                return
            }
            if exists {
                strongSelf.cancelRequest(link)
            }
            guard let url = URL(string: link) else {
                completion(nil, APIError.invalidURL)
                return
            }
            print("Started request for: \(link)")
            var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 15)
            request.allowsCellularAccess = true
            
            strongSelf.session.dataTask(with: request, completionHandler: { data, response, error in
                guard let _data = data, error == nil else {
                    if let _error = error as? NSError {
                        let apiError = ErrorCode.urlErrorDomain(_error)
                        print("NSError! \(apiError)")
                        completion(nil, apiError)
                        return
                    }
                    // If no error or response, there must be no internet connection
                    completion(nil, APIError.noInternet)
                    return
                }
                do {
                    guard let jsonArray = try JSONSerialization.jsonObject(with: _data, options: .allowFragments) as? [InfoDictionary] else {
                        
                        // Check the HTTP response if there's an error
                        if let httpResponse = response as? HTTPURLResponse, let apiError = ErrorCode.responseStatus(httpResponse) {
                            
                            completion(nil, apiError)
                        } else {
                            completion(nil, APIError.invalidData)
                        }
                        
                        return
                    }
                    completion(jsonArray, nil)
                } catch {
                    print("Error in parsing data into JSON: \(error)")
                    completion(nil, APIError.invalidData)
                }
            }).resume()
        }
    }
    
    func requestExists(_ urlString: String, exists: @escaping (Bool) -> Void) {
        self.session.getTasksWithCompletionHandler { dataTasks, _, _ in
            for task in dataTasks {
                if task.originalRequest?.url?.absoluteString == urlString {
                    exists(true)
                    break
                }
            }
            exists(false)
        }
    }
    
    func cancelRequest(_ urlString: String) {
        self.session.getTasksWithCompletionHandler { dataTasks, _, _ in
            for task in dataTasks {
                if task.originalRequest?.url?.absoluteString == urlString {
                    task.cancel()
                    print("Cancelled task: \"\(urlString)\"")
                    break
                }
            }
        }
    }
}

struct ErrorCode {
    static func urlErrorDomain(_ error: NSError) -> APIError {
        var apiError: APIError!
        switch error.code {
        // Invalid URL
        case NSURLErrorBadURL, NSURLErrorUnsupportedURL, NSURLErrorCannotFindHost, NSURLErrorDNSLookupFailed:
            apiError = APIError.invalidURL
            break
        // Request Time Out
        case NSURLErrorTimedOut, NSURLErrorCannotConnectToHost, NSURLErrorRequestBodyStreamExhausted, NSURLErrorBackgroundSessionWasDisconnected:
            apiError = APIError.requestTimeOut
            break
        // Invalid Data
        case NSURLErrorDataLengthExceedsMaximum, NSURLErrorResourceUnavailable, NSURLErrorZeroByteResource, NSURLErrorCannotDecodeRawData, NSURLErrorCannotDecodeContentData, NSURLErrorFileDoesNotExist, NSURLErrorFileIsDirectory, NSURLErrorNoPermissionsToReadFile:
            apiError = APIError.invalidData
            break
        // No Internet
        case NSURLErrorNetworkConnectionLost, NSURLErrorNotConnectedToInternet, NSURLErrorInternationalRoamingOff, NSURLErrorDataNotAllowed, NSURLErrorCannotLoadFromNetwork:
            apiError = APIError.noInternet
            break
        // Server error
        case NSURLErrorHTTPTooManyRedirects, NSURLErrorRedirectToNonExistentLocation, NSURLErrorBadServerResponse, NSURLErrorUserCancelledAuthentication, NSURLErrorUserAuthenticationRequired, NSURLErrorSecureConnectionFailed, NSURLErrorServerCertificateHasBadDate, NSURLErrorServerCertificateUntrusted, NSURLErrorServerCertificateHasUnknownRoot, NSURLErrorServerCertificateNotYetValid, NSURLErrorClientCertificateRejected, NSURLErrorClientCertificateRequired:
            apiError = APIError.serverError
            break
        default: // Other errors
            apiError = APIError.otherError
            break
        }
        return apiError
    }
    
    static func responseStatus(_ response: HTTPURLResponse) -> APIError? {
        var apiError: APIError?
        switch response.statusCodeEnum {
        case .ok: apiError = nil
            break
        case .requestTimeout, .gatewayTimeout, .nginxNoResponse:
            apiError = APIError.requestTimeOut
            break
        case .notModified, .badRequest, .notFound, .gone, .movedPermanently:
            apiError = APIError.invalidURL
            break
        case .internalServerError, .serviceUnavailable, .forbidden, .unauthorized, .badGateway:
            apiError = APIError.serverError
            break
        default: apiError = APIError.otherError
            break
        }
        return apiError
    }
}
