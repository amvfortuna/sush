//
//  AlbumTableViewCell.swift
//  Sush
//
//  Created by Anna Fortuna on 1/28/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var userName: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.title.text = "Unknown Title"
        self.userName.text = "Unknown User"
    }
}
