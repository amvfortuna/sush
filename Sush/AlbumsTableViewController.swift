//
//  AlbumsTableViewController.swift
//  Sush
//
//  Created by Anna Fortuna on 1/27/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import UIKit

class AlbumsTableViewController: UITableViewController {
    
    var api: API?
    var photosDetailView: PhotosViewController?
    var albums: [InfoDictionary]?
    fileprivate var isLoading = false

    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert((self.api != nil), "The API service should not be nil by this point.")
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.photosDetailView = (controllers[controllers.count-1] as! UINavigationController).topViewController as? PhotosViewController
            self.photosDetailView?.api = self.api
        }
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(AlbumsTableViewController.loadAlbums), for: .valueChanged)
        self.loadAlbums()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        
        // This will ensure that the UIRefreshControl will appear again once the Master View Controller reappeared.
        // See the comment in viewWillDisappear.
        if let rc = self.refreshControl, self.isLoading && !rc.isRefreshing {
            rc.beginRefreshing()
            self.tableView.setContentOffset(CGPoint(x: 0, y: self.tableView.contentOffset.y - rc.frame.size.height), animated: false)
        }
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // When the Master View Controller collapsed and the UIRefreshControl is still running, the spinner will be stuck when
        // the Master reappeared again. To prevent this, we have to call endRefreshing as the Master collapsed and call beginRefreshing
        // when it reappears.
        if let rc = self.refreshControl, rc.isRefreshing {
            rc.endRefreshing()
            self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        }
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail", let album = sender as? InfoDictionary {
            self.photosDetailView = (segue.destination as! UINavigationController).topViewController as? PhotosViewController
            self.photosDetailView?.api = self.api
            self.photosDetailView?.title = (album["title"] as! String).capitalized
            self.photosDetailView?.albumId = (album["id"] as! Int)
        }
    }
    
    // MARK: - API call
    
    func loadAlbums() {
        // Reset the albums array
        self.albums = nil
        self.isLoading = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.refreshControl?.beginRefreshing()
        self.tableView.reloadData()
        
        self.api!.getAlbumsWithUsers { [weak self] albums, error in
            self?.albums = albums
            self?.isLoading = false
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if error != nil {
                    var alert: UIAlertController?
                    switch error! {
                    case .noInternet: alert = Alerts.noInternet(); break
                    case .requestTimeOut: alert = Alerts.requestTimeOut(); break
                    case .serverError: alert = Alerts.serverError(); break
                    case .invalidData: alert = Alerts.invalidData(); break
                    case .invalidURL: alert = Alerts.invalidURL(); break
                    case .otherError: alert = Alerts.otherError(); break
                    }
                    self?.refreshControl?.endRefreshing()
                    self?.tableView.reloadData()
                    // To avoid the waring "presenting from detached view controllers", we can have the top most controller,
                    // which is the splitViewController itself, to present the alert.
                    self?.splitViewController?.present(alert!, animated: true, completion: nil)
                } else {
                    self?.refreshControl?.endRefreshing()
                    self?.tableView.reloadData()
                }
            }
        }
    }
}

// MARK: - Table view data source

extension AlbumsTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let rc = self.refreshControl else {
            return 1
        }
        
        if rc.isRefreshing {
            return 1
        } else {
            if self.albums == nil || self.albums!.count == 0 {
                return 1
            } else {
                return self.albums!.count
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let rc = self.refreshControl else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath)
            return cell
        }
        
        if rc.isRefreshing {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath)
            return cell
        } else {
            guard let albums = self.albums else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "errorCell", for: indexPath)
                return cell
            }
            if albums.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "noAlbums", for: indexPath)
                return cell
            } else {
                let album = albums[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath) as! AlbumTableViewCell
                if let title = album["title"] as? String, title != "" {
                    cell.title.text = title
                }
                if let user = album["user_name"] as? String, user != "" {
                    cell.userName.text = user
                }
                return cell
            }
        }
    }
}

// MARK: - Table view delegate

extension AlbumsTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let album = self.albums![indexPath.row]
        // To make sure that any modal presented by the current PhotosViewController was dismissed
        // before loading another PhotosViewController. This will prevent retaining previously 
        // allocated instances of PhotosViewController.
        if let _ = self.photosDetailView?.presentedViewController {
            self.photosDetailView?.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: "showDetail", sender: album)
            })
        } else {
            self.performSegue(withIdentifier: "showDetail", sender: album)
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // To remove extra rows
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
