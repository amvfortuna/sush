//
//  FullPhotoViewController.swift
//  Sush
//
//  Created by Anna Fortuna on 1/27/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import UIKit
import Kingfisher

class FullPhotoViewController: UIViewController {
    
    @IBOutlet weak var closeButton: UIButton! {
        didSet {
            closeButton.layer.borderColor = UIColor.sushBlue.cgColor
            closeButton.layer.borderWidth = 2
        }
    }
    @IBOutlet weak var fullImage: UIImageView! {
        didSet {
            self.loadImage()
        }
    }
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    var fullImageUrl: String?
    
    // MARK: - View lifecycle
    override func viewWillDisappear(_ animated: Bool) {
        print("Cancelling download of full size photo")
        self.fullImage.kf.cancelDownloadTask()
        super.viewWillDisappear(animated)
    }
    
    // MARK: - IBAction
    
    @IBAction func closeFullPhotoView() {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func retryLoadingImage() {
        self.messageLabel.isHidden = true
        self.retryButton.isHidden = true
        self.loadImage()
    }
    
    // MARK: - Loading the image
    
    fileprivate func loadImage() {
        if let _fullImageUrl = self.fullImageUrl, let resourceUrl = URL(string: _fullImageUrl) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            fullImage.kf.indicatorType = .activity
            fullImage.kf.setImage(with: resourceUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { [weak self] _, error, _, _ in
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    if error != nil {
                        self?.messageLabel.isHidden = false
                        self?.messageLabel.text = error!.localizedDescription
                        self?.retryButton.isHidden = false
                    }
                }
            })
        } else {
            self.messageLabel.isHidden = false
            self.messageLabel.text = "The link for this image appears to be broken."
            self.retryButton.isHidden = false
        }
    }
}
