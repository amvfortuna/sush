//
//  PhotosViewController.swift
//  Sush
//
//  Created by Anna Fortuna on 1/27/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import UIKit
import Kingfisher

class PhotosViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var messageLabel: UILabel! {
        didSet {
            // Show the welcome message.
            if self.albumId == nil {
                let greetingRange = (Text.WelcomeMessage as NSString).range(of: "Hi there!")
                let listNameRange = (Text.WelcomeMessage as NSString).range(of: "Albums")
                let mainAttributes = Text.attributes(colour: .black, withSize: FontSize.Body)
                let greetingAttributes = Text.attributes(colour: .sushBlue, withSize: FontSize.Title)
                let highlightAttributes = Text.attributes(colour: .sushBlue, withSize: FontSize.Body)
                let attributedString = NSMutableAttributedString(string: Text.WelcomeMessage, attributes: mainAttributes)
                attributedString.addAttributes(greetingAttributes, range: greetingRange)
                attributedString.addAttributes(highlightAttributes, range: listNameRange)
                messageLabel.attributedText = attributedString
            }
        }
    }
    @IBOutlet weak var retryButton: UIButton!
    fileprivate var refreshControl: UIRefreshControl?
    var api: API!
    var albumId: Int?
    var photos: [InfoDictionary]?
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert((self.api != nil), "The API service cannot be nil by this point")
        self.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
        self.navigationItem.leftItemsSupplementBackButton = true
        self.loadPhotosFromAlbum()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(PhotosViewController.loadPhotosFromAlbum), for: .valueChanged)
        self.collectionView.addSubview(self.refreshControl!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let _albumId = self.albumId {
            self.api.cancelRequest(self.api.endpoint.Photos + "\(_albumId)")
            print("Initiated cancel request for: \(self.api.endpoint.Photos)\(_albumId)")
        }
    }
    
    // MARK: - API and view config
    
    func loadPhotosFromAlbum() {
        if let _albumId = self.albumId {
            // Reset the photos array
            self.photos = nil
            self.refreshControl?.beginRefreshing()
            self.messageLabel.isHidden = true
            self.retryButton.isHidden = true
            self.collectionView.isHidden = true
            self.loading.startAnimating()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            self.api!.getPhotosFromAlbum(id: _albumId) { [weak self] photos, error in
                guard let _photos = photos, error == nil else {
                    self?.showError(error!)
                    return
                }
                if _photos.count > 0 {
                    self?.photos = _photos
                    self?.showPhotos()
                } else {
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self?.refreshControl?.endRefreshing()
                        self?.loading.stopAnimating()
                        self?.messageLabel.attributedText = Text.compose("No Photos available.", "Why don't you check out the other albums.")
                        self?.messageLabel.isHidden = false
                    }
                }
            }
        }
    }
    
    fileprivate func showPhotos() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.refreshControl?.endRefreshing()
            self.messageLabel.isHidden = true
            self.collectionView.reloadData()
            self.loading.stopAnimating()
            self.collectionView.isHidden = false
        }
    }
    
    fileprivate func showError(_ error: APIError) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.refreshControl?.endRefreshing()
            switch error {
            case .noInternet:
                self.messageLabel.attributedText = Text.Error.noInternetMessage()
                break
            case .requestTimeOut:
                self.messageLabel.attributedText = Text.Error.requestTimeOutMessage()
                break
            case .serverError:
                self.messageLabel.attributedText = Text.Error.serverErrorMessage()
                break
            case .invalidData:
                self.messageLabel.attributedText = Text.Error.invalidDataMessage()
                break
            case .invalidURL:
                self.messageLabel.attributedText = Text.Error.invalidURLMessage()
                break
            case .otherError:
                self.messageLabel.attributedText = Text.Error.otherErrorMessage()
                break
            }
            self.loading.stopAnimating()
            self.messageLabel.isHidden = false
            self.retryButton.isHidden = false
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFullImage" {
            if let indexPath = self.collectionView.indexPathsForSelectedItems {
                let photoInfo = self.photos![(indexPath.last?.row)!]
                let fullPhotoController = segue.destination as! FullPhotoViewController
                fullPhotoController.fullImageUrl = (photoInfo["url"] as! String)
            }
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func retryLoadingPhotos() {
        self.loadPhotosFromAlbum()
    }
}

// MARK: - Collection view data source

extension PhotosViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photosCell", for: indexPath) as! PhotosCollectionViewCell
        if let photos = self.photos?[indexPath.row], let url = photos["thumbnailUrl"] as? String {
            cell.thumbnail.kf.setImage(with: URL(string: url), placeholder: UIImage(named: "album-placeholder"), options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: nil)
        }
        return cell
    }
}

// MARK: - Collection view delegate

extension PhotosViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as! PhotosCollectionViewCell).thumbnail.kf.cancelDownloadTask()
    }
}
