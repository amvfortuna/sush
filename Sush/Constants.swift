//
//  Constants.swift
//  Sush
//
//  Created by Anna Fortuna on 1/28/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import Foundation
import UIKit

struct Text {
    static let WelcomeMessage = "Hi there!\n\n To start, please select an album from the Albums list."
    
    struct Error {
        static let NoInternetTitle = "No Internet Connection."
        static let NoInternetBody = "Please make sure your device is connected to a WiFi network. Otherwise please turn on your mobile data network if available."
        static let RequestTimeoutTitle = "Sorry, the request timed out."
        static let RequestTimeoutBody = "Looks like the server is taking to long to respond, please try again sometime."
        static let ServerErrorTitle = "An internal server error occurred."
        static let ServerErrorBody = "The server encountered a temporary error and could not complete your request."
        static let InvalidDataTitle = "The data we received is not valid."
        static let InvalidDataBody = "Either the link is broken or there's a problem with the server. Please try again."
        static let InvalidURLTitle = "The link is broken."
        static let InvalidURLBody = "Looks like the link we're trying to access is no longer valid."
        static let OtherErrorTitle = "Unable to complete request."
        static let OtherErrorBody = "An error occurred while your request was being processed. Please try again."
        
        static func noInternetMessage() -> NSAttributedString {
            return Text.compose(NoInternetTitle, NoInternetBody)
        }
        
        static func requestTimeOutMessage() -> NSAttributedString {
            return Text.compose(RequestTimeoutTitle, RequestTimeoutBody)
        }
        
        static func serverErrorMessage() -> NSAttributedString {
            return Text.compose(ServerErrorTitle, ServerErrorBody)
        }
        
        static func invalidDataMessage() -> NSAttributedString {
            return Text.compose(InvalidDataTitle, InvalidDataBody)
        }
        
        static func invalidURLMessage() -> NSAttributedString {
            return Text.compose(InvalidURLTitle, InvalidURLBody)
        }
        
        static func otherErrorMessage() -> NSAttributedString {
            return Text.compose(OtherErrorTitle, OtherErrorBody)
        }
    }
    
    static func compose(_ title: String, _ body: String) -> NSAttributedString {
        let message = NSMutableAttributedString()
        let _title = NSAttributedString(string: title + "\n\n", attributes: Text.attributes(colour: .sushBlue, withSize: FontSize.Title))
        let _body = NSAttributedString(string: body, attributes: Text.attributes(colour: .black, withSize: FontSize.Body))
        message.append(_title)
        message.append(_body)
        return message
    }
    
    static func attributes(colour: UIColor, withSize size: CGFloat) -> [String: Any] {
        return [NSForegroundColorAttributeName: colour, NSFontAttributeName: UIFont.systemFont(ofSize: size)]
    }
}

struct FontSize {
    static let Title: CGFloat = {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return 25
        } else {
            return 35
        }
    }()
    
    static let Body: CGFloat = {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return 17
        } else {
            return 22
        }
    }()
}
