//
//  Global.swift
//  Sush
//
//  Created by Anna Fortuna on 1/27/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import Foundation

func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG_VERSION
        Swift.print(items[0], separator:separator, terminator: terminator)
    #endif
}
