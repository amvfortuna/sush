//
//  APITests.swift
//  Sush
//
//  Created by Anna Fortuna on 1/29/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import XCTest
@testable import Sush

class APITests: XCTestCase {
    
    var api: API! = nil
    
    override func setUp() {
        super.setUp()
        self.api = API()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        self.api = nil
    }
    
    func testInvalidURL() {
        let fetchAlbumsExpectation = expectation(description: "Send a fetch request with an invalid URL")
        
        self.api.get("this is not a valid url") { results, error in
            XCTAssertNil(results)
            XCTAssertNotNil(error)
            XCTAssertTrue(error! == APIError.invalidURL)
            fetchAlbumsExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testCorrectEndpoint() {
        let fetchAlbumsExpectation = expectation(description: "Fetch albums")
        
        self.api.get("http://jsonplaceholder.typicode.com/albums") { results, error in
            XCTAssertNotNil(results)
            XCTAssertNil(error)
            fetchAlbumsExpectation.fulfill()
        }

        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testIncorrectEndpoint() {
        let fetchAlbumsExpectation = expectation(description: "Send a fetch request with an incorrect endpoint")
        
        self.api.get("http://jsonplaceholder.typicode.com/photosxxx") { results, error in
            XCTAssertNil(results)
            XCTAssertNotNil(error)
            XCTAssertTrue(error! == APIError.invalidURL)
            fetchAlbumsExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testIncorrectEndpointParameter() {
        let fetchAlbumsExpectation = expectation(description: "Send a fetch request with a correct endpoint but incorrect parameter/s")
        
        self.api.get("http://jsonplaceholder.typicode.com/albums?id=hello") { results, error in
            XCTAssertNotNil(results)
            XCTAssertNil(error)
            XCTAssertTrue(results!.count == 0)
            fetchAlbumsExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testCompleteAlbumList() {
        let fetchExpectation = expectation(description: "Send a fetch request with correct album and user list")
        
        self.api.get("http://jsonplaceholder.typicode.com/albums") { albums, albumError in
            XCTAssertNotNil(albums)
            self.api.get("http://jsonplaceholder.typicode.com/users", completion: { users, userError in
                XCTAssertNotNil(users)
                var list = [InfoDictionary]()
                albumLoop: for album in albums! {
                    var _album = album
                    _album["user_name"] = ""
                    userLoop: for user in users! {
                        if (user["id"] as! Int) == (album["userId"] as? Int) {
                            _album["user_name"] = user["name"] as! String
                            break userLoop
                        }
                    }
                    list.append(_album)
                }
                XCTAssertTrue(list.count > 0)
                fetchExpectation.fulfill()
            })
        }
        
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testCancelRequest() {
        let exp = expectation(description: "Cancel an existing request")
        
        self.api.get("http://jsonplaceholder.typicode.com/albums") { _, _ in }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.api.cancelRequest("http://jsonplaceholder.typicode.com/albums")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.api.requestExists("http://jsonplaceholder.typicode.com/albums", exists: { exists in
                    XCTAssertFalse(exists, "The request should already be cancelled.")
                    exp.fulfill()
                })
            }
        }
        waitForExpectations(timeout: 15, handler: nil)
    }
}
