//
//  SushTests.swift
//  Sush
//
//  Created by Anna Fortuna on 1/30/17.
//  Copyright © 2017 Anna Fortuna. All rights reserved.
//

import XCTest
@testable import Sush

class SushTests: XCTestCase {
    
    var albumViewController: AlbumsTableViewController! = nil
    var photosViewController: PhotosViewController! = nil
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.albumViewController = storyboard.instantiateViewController(withIdentifier: "Albums Controller") as! AlbumsTableViewController
        self.photosViewController = storyboard.instantiateViewController(withIdentifier: "Photos Controller") as! PhotosViewController
        self.albumViewController.api = API()
        self.photosViewController.api = self.albumViewController.api
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        self.albumViewController.albums = nil
        self.photosViewController.photos = nil
        self.albumViewController.api = nil
        self.photosViewController.api = nil
    }
    
    func testLoadAlbumsFromAPISuccessfully() {
        let _ = self.albumViewController.view
        let exp = expectation(description: "Fetching albums from the API.")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            XCTAssertNotNil(self.albumViewController.albums, "The albums array should not be nil")
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 15) { error in
            if error != nil {
                print("Expectation timed out with error: \(error)")
            }
        }
    }
    
    func testLoadAlbumsFromAPIWithTimeOut() {
        let _ = self.albumViewController.view
        let exp = expectation(description: "Fetching albums from the API with request time out.")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            XCTAssertNil(self.albumViewController.albums, "The albums array should remain nil")
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 15) { error in
            if error != nil {
                print("Expectation timed out with error: \(error)")
            }
        }
    }
    
    func testAlbumTableCellShouldDisplayCorrectContent() {
        let _ = self.albumViewController.view
        XCTAssertNotNil(self.albumViewController.tableView, "The tableView must be loaded by now.")
        let exp = expectation(description: "Fetching albums from the API.")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
            let indexPathForVisibleRows = self.albumViewController.tableView.indexPathsForVisibleRows
            XCTAssertNotNil(indexPathForVisibleRows, "At least one row should be visible")
            
            if self.albumViewController.albums == nil {
                let cell = self.albumViewController.tableView.cellForRow(at: IndexPath(row: 0, section: 0))
                XCTAssertNotNil(cell, "A tableViewCell must be visible")
                XCTAssertTrue(cell?.reuseIdentifier == "errorCell", "Since the albums array is nil, the error cell must be displayed")
            } else {
                for indexPath in indexPathForVisibleRows! {
                    let cell = self.albumViewController.tableView.cellForRow(at: indexPath)
                    if let albumCell = cell as? AlbumTableViewCell {
                        let content = self.albumViewController.albums![indexPath.row]
                        XCTAssertEqual(albumCell.title.text, (content["title"] as! String))
                        XCTAssertEqual(albumCell.userName.text, (content["user_name"] as! String))
                    } else if let identifier = cell?.reuseIdentifier {
                        XCTAssertTrue(identifier == "noAlbums", "The cell should display No Albums if the request was successful but no data was returned.")
                    } else {
                        XCTAssertNil(cell, "These cells are not yet reused")
                    }
                }
            }
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 20) { error in
            print("Expectation timed out with error: \(error)")
        }
    }
    
    func testLoadPhotosFromAPISuccessfully() {
        let _ = self.photosViewController.view
        self.photosViewController.albumId = 1
        self.photosViewController.loadPhotosFromAlbum()
        let exp = expectation(description: "Fetching photos from the API.")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            XCTAssertNotNil(self.photosViewController.photos, "The photos array should not be nil")
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 15) { error in
            if error != nil {
                print("Expectation timed out with error: \(error)")
            }
        }
    }
    
    func testLoadPhotosFromAPIWithTimeOut() {
        let _ = self.photosViewController.view
        self.photosViewController.albumId = 1
        self.photosViewController.loadPhotosFromAlbum()
        let exp = expectation(description: "Fetching photos from the API with request time out.")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            XCTAssertNil(self.photosViewController.photos, "The photos array should remain nil")
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 15) { error in
            if error != nil {
                print("Expectation timed out with error: \(error)")
            }
        }
    }
    
    func testPhotoCollectionViewCellShouldDisplayCorrectContent() {
        let _ = self.photosViewController.view
        XCTAssertNotNil(self.photosViewController.collectionView, "The collection view must be loaded by now.")
        self.photosViewController.albumId = 1
        self.photosViewController.loadPhotosFromAlbum()
        let exp = expectation(description: "Fetching photos from the API.")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
            let indexPathForVisibleCells = self.photosViewController.collectionView.indexPathsForVisibleItems
            XCTAssertNotNil(indexPathForVisibleCells, "At least one item should be visible")
            
            if self.photosViewController.photos == nil {
                XCTAssertFalse(self.photosViewController.messageLabel.isHidden, "The error message should be displayed.")
                XCTAssertTrue(self.photosViewController.collectionView.isHidden, "The collection view must be hidden.")
            } else {
                for indexPath in indexPathForVisibleCells {
                    let cell = self.photosViewController.collectionView.cellForItem(at: indexPath)
                    if let photosCell = cell as? PhotosCollectionViewCell {
                        let content = self.photosViewController.photos![indexPath.row]
                        let url = (content["thumbnailUrl"] as! String)
                        XCTAssertEqual(photosCell.thumbnail.kf.webURL?.absoluteString, url, "The cell should be loading the correct url")
                    } else {
                        XCTFail("Unknown collection view cell")
                    }
                }
            }
            exp.fulfill()
        }
        
        waitForExpectations(timeout: 20) { error in
            if error != nil {
                print("Expectation timed out with error: \(error)")
            }
        }
    }
}
